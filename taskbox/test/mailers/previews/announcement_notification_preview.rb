# Preview all emails at http://localhost:3000/rails/mailers/announcement_notification
class AnnouncementNotificationPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/announcement_notification/notify
  def notify
    AnnouncementNotification.notify
  end

end
