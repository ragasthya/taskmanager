25.times do
	client = Client.new
	first_name = Faker::Name.first_name
	last_name = Faker::Name.last_name
	client.name = first_name + " " + last_name
	client.company = Faker::Company.name
	client.email = Faker::Internet.free_email(first_name)
	client.mobile_number = Faker::Number.number(10)
	client.website = "www." + Faker::Internet.domain_name
	client.save
end

150.times do
	project = Project.new
	project.name = Faker::App.name
	project.description = Faker::Lorem.sentence
	project.start_date = Faker::Date.between(Date.today.beginning_of_year, Date.today.end_of_year)
	project.end_date = Faker::Date.between((Date.today + 2.year).beginning_of_year, (Date.today + 2.year).end_of_year)
	project.client_id = Client.all.pluck(:id).sample
	project.save
end

index = 4
150.times do
	if index > 153
		break
	end
	project = Project.find(index)
	project.update_attributes(budget: Faker::Number.number(4))
	index += 1
end

index = 4
150.times do
	if index > 153
		break
	end
	project = Project.find(index)
	project.update_attributes(status: ["New", "Completed", "In Progress"].sample)
	index += 1
end


300.times do
	task = Task.new
	task.title = Faker::Hacker.ingverb
	task.due_date = Faker::Date.between(Date.today + 1.month, Date.today + 7.month)
	task.is_completed = [true, false].sample
	task.project_id = Project.all.pluck(:id).sample
	task.save
end

Task.all.each do |task|
	task.due_date = Faker::Date.between(Date.today - 6.month, Date.today + 6.month)
	task.save
end