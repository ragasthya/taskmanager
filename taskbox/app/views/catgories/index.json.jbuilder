json.array!(@catgories) do |catgory|
  json.extract! catgory, :id, :name, :user_id
  json.url catgory_url(catgory, format: :json)
end
