class TasksController < ApplicationController

	before_filter :authenticate_user!
	load_and_authorize_resource
	
	def index
		@tasks = Task.all
	end

	def create
		@task = Task.new(task_params)
		if @task.save
			redirect_to project_path(@task.project_id), notice: "Successfully created the task!"
		end
	end

	def show
		@task = Task.find(params[:id])
	end

	def mark_as_complete
		task = Task.find(params[:task_id])
		task.update_attributes(is_completed: true)
		Notification.completed(task, current_user).deliver!
		redirect_to :back, notice: "Successfully marked task as complete."
	end

	def mark_as_incomplete
		task = Task.find(params[:task_id])
		task.update_attributes(is_completed: false)
		Notification.incomplete(task, current_user).deliver!
		redirect_to :back, notice: "Successfully marked task as incomplete."
	end

	private
	def task_params
		params[:task].permit(:title, :due_date, :is_completed, :project_id)
	end
end
