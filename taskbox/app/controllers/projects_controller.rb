class ProjectsController < ApplicationController

	before_filter :authenticate_user!
	load_and_authorize_resource
	
	def index 
		if params[:status]
			@projects = current_user.projects.where('status = ?',params[:status])
		else
			@projects = Project.all
		end

		if params[:id]
			@projects = current_user.projects.where('client_id = ?',params[:id])
		else
			@projects = current_user.projects
		end
	end

	def new
		@client = Client.find(params[:client_id])
		@project = Project.new
		@project.client_id = @client.id
	end

	def create
		@project = Project.new(project_params)
		@project.user_id = current_user.id
		if @project.save
			redirect_to client_path(@project.client_id)
		else
			render action: "new"
		end
	end

	def show
		begin
			@project = Project.find(params[:id])
			@task = Task.new
			@complete_tasks = Task.list_by_complete.where('project_id = ?', @project.id)
			@incomplete_tasks = Task.list_by_incomplete.where('project_id = ?', @project.id)
			@overdue_tasks = Task.list_by_overdue.where('project_id = ?', @project.id)
		rescue ActiveRecord::RecordNotFound
			redirect_to projects_path, notice: "Project Not Found!"
		end
	end

	def edit
		@project = Project.find(params[:id])
	end

	def update
		@project = Project.find(params[:id])
		@project.user_id = current_user.id
		if @project.update_attributes(project_params)
			redirect_to client_path(@project.client_id), notice: "Successfully Updated!"
		else
			render action: "edit"
		end
	end

	def destroy
		@project = Project.find(params[:id])
		@project.destroy
		redirect_to client_path(@project.client_id), notice: "Successfully destroyed #{@project.name}'s records."
	end

	private
	def project_params
		#This paramter hash is available in the object params.
		params[:project].permit(:name, :description, :start_date, :status, :end_date, :client_id, :estimated_budget, catgory_ids: [])
	end
end
