class SayController < ApplicationController

  authorize_resource class: false
  def hello
  end

  def goodbye
  end

  def products
  	@products = {		"Surface Pro" => ["Microsoft", 12.3, 128, "Windows 10", 849.99], 
						"Galaxy Tab S2" => ["Samsung", 8, 32, "Android 6 Marshmello", 299.99], 
						"Fire" => ["Amazon", 7, 8, "Android Open Source: Amazon", 49.99],
						"iPad Air 2" => ["Apple", 9.7, 64, "iOS 8.1", 499.99]	}
  end
end
