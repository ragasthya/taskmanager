class SandboxController < ApplicationController
  def clients
  	@clients = Client.all
  end

  def projects
  	#@projects = Project.all
  	#@projects = Project.where('client_id = ?', 40)
  	#@projects = Project.where('start_date < ?', Date.today)
  	#@projects = Project.order('client_id')
  	#@projects = Project.order('start_date')
  	#@projects = Project.order('start_date DESC').limit(50)
  	@projects = Project.order('name').order('client_id').order('start_date DESC').order('end_date')
  	#Delete based on a Condition: Project.where('client_id = 5').delete_all
  end
end 