class AnnouncementsController < ApplicationController
	before_filter :authenticate_user!
	load_and_authorize_resource

	def index
		@announcements = current_user.announcements
	end

	def new
		@announcement = Announcement.new
	end

	def create
		@announcement = Announcement.new(announcement_params)
		@announcement.user_id = current_user.id
		if @announcement.save
			redirect_to announcements_path
			AnnouncementNotification.notify(@announcement, current_user.clients.pluck(:name).join(", "))
		else
			render action: new
		end
	end

	def show
		begin 
			@announcement = current_user.find(params[:id])
		rescue ActiveRecord::RecordNotFound
			redirect_to announcements_path, notice: "Announcement not found!"
		end
	end

	def edit
		@announcement = current_user.announcements.find(params[:id])
	end

	def update
		@announcement = current_user.announcements.find(params[:id])
		@announcement.user_id = current_user.id
		if @announcement.update_attributes(announcement_params)
			redirect_to announcement_path(@announcement.id), notice: "Successfully Updated!"
		else
			render action: "edit"
		end
	end

	def destroy
		@announcement = current_user.announcements.find(params[:id])
		@announcement.destroy
		redirect_to announcements_path, notice: "Successfully destroyed #{@announcement.title}'s records."
	end

	private
	def announcement_params
		#This paramter hash is available in the object params.
		params[:announcement].permit(:title, :content)
	end
end

