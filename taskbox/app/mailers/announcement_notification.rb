class AnnouncementNotification < ActionMailer::Base
  default from: "agasthya.rahul1@gmail.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.announcement_notification.notify.subject
  #
  def notify(announcement, clients)
    @announcement = announcement
    @clients = clients
    mail to: "agasthya.rahul1@gmail.com", subject: @announcement.title
    #cc and bcc can be added to the above field. Usually to: "#{task.project.client.email}"
  end
end
