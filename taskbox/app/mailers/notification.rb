class Notification < ActionMailer::Base

  default from: "agasthya.rahul1@gmail.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notification.completed.subject
  #
  def completed(task,user)
    @task = task
    @user = user
    mail to: "agasthya.rahul1@gmail.com", subject: "Task Completion Update. DO NOT REPLY."
    #cc and bcc can be added to the above field. Usually to: "#{task.project.client.email}"
  end

  def incomplete(task,user)
    @task = task
    @user = user
    mail to: "agasthya.rahul1@gmail.com", subject: "Task Completion Update. DO NOT REPLY."
    #cc and bcc can be added to the above field. Usually to: "#{task.project.client.email}"
  end
end
