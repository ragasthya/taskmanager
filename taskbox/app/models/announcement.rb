class Announcement < ActiveRecord::Base
	belongs_to :user
	validates_presence_of :title, :content
	validates_length_of :content, minimum: 10
end
