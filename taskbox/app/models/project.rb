class Project < ActiveRecord::Base

	belongs_to :client
	belongs_to :project
	has_many :tasks

	has_many :project_catgories
	has_many :catgories, through: :project_catgories

	before_destroy :destroy_tasks

	validates_presence_of :name, :description, :start_date, :end_date, :estimated_budget, :status
	validates_length_of :description, minimum: 10

	validate :check_date

	def self.new_projects
		Project.where('status = ?', 'New')
	end

	def self.all_projects
		Project.all
	end

	def list_tasks
		Task.where('project_id = ?', self.id)
	end

	def details
		"<h3>NAME</h3> #{self.name} <br/>" + 
		"<h3>CLIENT</h3> <%= link_to #{Client.find(self.client_id).name}, client_path(project.client_id) %> <br/>" + 
		"<h3>DESCRIPTION</h3> #{self.description} <br/>" + 
		"<h3>START DATE</h3> #{self.start_date.strftime("%A, %d %B %Y")} <br/>" +
		"<h3>END DATE</h3>#{self.end_date.strftime("%A, %d %B %Y")}<br/>"
	end

	def destroy_tasks
		self.tasks.each do |task|
			task.destroy
		end
	end

	private 
	def check_date
		if self.end_date <= self.start_date
			errors.add(:end_date, "should be greater than Start Date")
		end
	end
end
