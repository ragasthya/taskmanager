class Catgory < ActiveRecord::Base
	belongs_to :user

	has_many :project_catgories
	has_many :projects, through: :project_catgories

	validates_presence_of :name
end
