class Client < ActiveRecord::Base

	has_many :projects
	belongs_to :user

	before_create :generate_code
	before_destroy :destroy_projects

	#Validations
	validates_presence_of :name, :company, :email, :mobile_number
	validates_length_of :mobile_number, is: 10
	validates_numericality_of :mobile_number
	validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
	validates_uniqueness_of :mobile_number, :email, scope: :user_id

	def list_projects
		Project.where('client_id = ?', self.id)
	end

	def list_by_status(status)
		Project.where('client_id = ? AND status = ?', self.id, status)
	end

	def details
		"NAME: #{self.name} <br>" +
		"COMPANY: #{self.company} <br>" +
		"EMAIL: #{self.email} <br>" +
		"MOBILE: #{self.mobile_number}"
	end

	def generate_code
		self.code = "#{self.name[0..3].upcase}#{Random.rand(10000)}"
	end

	def destroy_projects
		self.projects.each do |project|
			project.destroy
		end
	end
end
