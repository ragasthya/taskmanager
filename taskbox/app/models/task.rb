class Task < ActiveRecord::Base
	belongs_to :project

	validates_presence_of :title, :due_date
	#validates_numericality_of :project_id

	#validate :check_due_date

	def self.list_by_complete
		Task.where('is_completed = ?', true)
	end

	def self.list_by_incomplete
		Task.where('is_completed = ?', false)
	end

	def self.list_by_overdue
		Task.where('due_date < ?', Date.today)
	end

	private

	def check_due_date
		if (!self.due_date.nil?) && (self.due_date > project.end_date || self.due_date < project.start_date)
			errors.add(:due_date, "should be between #{project.start_date} and #{project.end_date}")
		end
	end
end
