class CreateCatgories < ActiveRecord::Migration
  def change
    create_table :catgories do |t|
      t.string :name
      t.integer :user_id

      t.timestamps
    end
  end
end
