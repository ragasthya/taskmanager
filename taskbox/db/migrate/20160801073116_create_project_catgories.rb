class CreateProjectCatgories < ActiveRecord::Migration
  def change
    create_table :project_catgories do |t|
      t.integer :project_id
      t.integer :catgory_id

      t.timestamps
    end
  end
end
