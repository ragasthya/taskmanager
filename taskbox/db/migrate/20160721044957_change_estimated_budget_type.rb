class ChangeEstimatedBudgetType < ActiveRecord::Migration
  def change
  	change_column :projects, :estimated_budget, :double
  end
end
