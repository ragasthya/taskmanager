class ChangeColumnTypesProjects < ActiveRecord::Migration
  def change
  	change_column :projects, :estimated_budget, :string
  end
end
