class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
    	t.string :name
    	t.string :company
    	t.string :mobile_number
    	t.string :email
    	t.string :website

    	t.timestamps null: false
=begin
Every table will have two additional columns, if t.timestamps null: false is present:
	t.datetime :created_at
	t.datetime :updated_at
Other data types include:
integer, float, text, boolean, string, date, time, datetime
=end
    end
  end
end